"use strict";

//= vendor/jquery.min.js
//= ../node_modules/smooth-scroll/dist/smooth-scroll.polyfills.js
//= ../node_modules/magnific-popup/dist/jquery.magnific-popup.js
//= ../node_modules/mobile-detect/mobile-detect.js
//= ../node_modules/slick-carousel/slick/slick.js
//= ../node_modules/retinajs/dist/retina.js

var app = (function () {
    var md = new MobileDetect(window.navigator.userAgent);
	return {
		init: function () {
			app.lodgements();
			app.categoryToggle();
			app.subCategoryToggle();
			app.initPopup();
			app.popupClose();
			app.initMenu();
		},
        lodgements: function () {
            $(".js-lodgements__toggle").click(function() {
                var $category =  $(this).closest(".js-category");
                $category.toggleClass("open");
                $('.js-lodgements').toggleClass("open");
            })
        },
        categoryToggle: function () {
            $(".js-category").click(function() {
                var $category =  $(this),
                    $root =  $(this).closest(".js-category-root"),
                    $products = $root.find(".js-products.open"),
                    $subCategoryToggles = $root.find(".js-subcategory.open");
                $category.toggleClass("open");
                $subCategoryToggles.toggleClass("open");
                $products.toggleClass("open");
                $category.next().toggleClass("open");
            })
        },
        subCategoryToggle: function () {
            $(document).on("click", ".js-subcategory", function(event) {
                var $root =  $(this).closest(".js-category-root"),
                    $subCategoryToggle = $(this),
                    $subCategoryToggles = $root.find(".js-subcategory"),
                    $products = $root.find(".js-products"),
                    $subCategory = $root.find(".js-"+ $subCategoryToggle.data("color"));
                if(md.mobile()){
                    var $insert = $(this).next();
                    //зачищаем все места вставки
                    $($root.find(".js-insert-products")).html('');


                    if($subCategoryToggle.hasClass("open")){
                        $subCategoryToggle.toggleClass("open");
                        $insert.toggleClass("open");
                    } else {
                        $($root.find(".js-insert-products")).removeClass("open");
                        $insert.toggleClass("open");
                        $($subCategory).clone().appendTo($insert);
                        var $carousel = $insert.find(".js-products-carousel"),
                            $insertedProducts = $insert.find(".js-products");
                        //
                        $carousel.css({
                            height: '1px',
                            overflow: 'hidden',
                            visibility: 'hidden',
                            transition: 'height 0.2s linear'
                        });
                        $carousel.slick({
                            slidesToShow: 2,
                            prevArrow: '<button id="prev" type="button" class="slick__btn slick__btn--prev"></button>',
                            nextArrow: '<button id="next" type="button" class="slick__btn slick__btn--next"></button>',
                            responsive: [
                                {
                                    breakpoint: 1024,
                                    settings: {
                                        slidesToShow: 3,
                                        slidesToScroll: 1
                                    }
                                },
                                {
                                    breakpoint: 767,
                                    settings: {
                                        slidesToShow: 2,
                                        slidesToScroll: 1
                                    }
                                },
                                {
                                    breakpoint: 400,
                                    settings: {
                                        slidesToShow: 1,
                                        slidesToScroll: 1
                                    }
                                }
                                // You can unslick at a given breakpoint now by adding:
                                // settings: "unslick"
                                // instead of a settings object
                            ]
                        });

                        $carousel.css({
                            height: 'auto',
                            visibility: 'visible',
                        });
                        $carousel.slick('slickNext');
                        $subCategoryToggles.removeClass("open");
                        $insertedProducts.toggleClass("open");
                        $subCategoryToggle.toggleClass("open");

                    }

                } else{
                    $subCategoryToggles.removeClass("open");
                    $products.removeClass("open");
                    $subCategoryToggle.toggleClass("open");
                    $subCategory.toggleClass("open");
                }
            })
        },
        initPopup: function () {
            $(document).on("click", ".js-popup", function(event) {
                $.magnificPopup.open({
                    items: {
                        src: '#modal', // can be a HTML string, jQuery object, or CSS selector
                        type: 'inline'
                    }
                })
            });

        },
        popupClose: function () {
            $('.js-mfp-close').click(function () {
                var magnificPopup = $.magnificPopup.instance;
                magnificPopup.close();
            });
        },
        initMenu: function () {
            $('.js-menu-link').click(function (e) {
                e.preventDefault();
                var el = $(this).attr('href');
                $(el).addClass('open');
                console.log(el);
            });
            var smoothScroll = new SmoothScroll('a[href*="#"]');
            if(md.mobile()){
                $(".js-header__menu-icon").click(function () {
                    $(".js-header-menu").show();
                });
                $(".js-header__menu-close, .js-menu-link").click(function () {
                    $(".js-header-menu").hide();
                });

            }

        },

	}

})();

$(document).on('ready', app.init);






